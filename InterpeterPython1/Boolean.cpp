#include "Boolean.h"

Boolean::Boolean(bool b)
{
	this->_b = b;
}

Boolean::~Boolean()
{

}

bool const Boolean::isPrintable() 
{
	return true;
}

std::string const Boolean::toString() 
{
	return this->_b ? "true" : "false";
}