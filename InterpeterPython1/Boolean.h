#ifndef BOOLEAN_H
#define BOOLEAN_H
#include "type.h"

class Boolean : public Type
{
public:
	Boolean(bool b);
	~Boolean();
	virtual const bool isPrintable();
	virtual const std::string toString();
private:
	bool _b;
};
#endif // BOOLEAN_H