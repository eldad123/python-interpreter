#ifndef INTEGER_H
#define INTEGER_H
#include "type.h"

class Integer : public Type
{
public:
	Integer(int x);
	~Integer();
	int getX();
	void setX(int x);
	virtual const bool isPrintable();
	virtual const std::string toString();
private:
	int _x;
};
#endif // INTEGER_H