#ifndef NAME_ERROR_EXCEPTION_H
#define NAME_ERROR_EXCEPTION_H
#include <iostream>
#include "InterperterException.h"

class NameErrorException : public InterperterException
{
public:
	NameErrorException(const std::string str); //c'tor
	virtual const char* what() const throw(); //exception

private:
	std::string _name; //name
};
#endif // NAME_ERROR_EXCEPTION_H