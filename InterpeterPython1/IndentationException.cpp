#include "IndentationException.h"

//exception of indentation
const char* IndentationException::what() const throw()
{
	return "IndentationError: unexpected indent";
}