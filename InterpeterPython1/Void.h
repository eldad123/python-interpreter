#ifndef VOID_H
#define VOID_H
#include "type.h"

class Void : public Type
{
public:
	Void();
	~Void();
	virtual const bool isPrintable();
	virtual const std::string toString();
};
#endif // VOID_H