#ifndef STRING_H
#define STRING_H
#include "Sequence.h"

class String : public Sequence
{
public:
	String(std::string str);
	~String();
	virtual const bool isPrintable();
	virtual const std::string toString();
private:
	std::string _str;
};

#endif // STRING_H