#ifndef PARSER_H
#define PARSER_H

#include "InterperterException.h"
#include "type.h"
#include "Helper.h"
#include <string>
#include <unordered_map>
#include <iostream>
#include <sstream>
#include <unordered_map>

class Parser
{
public:
	static Type* parseString(std::string str) throw(); //check input
	static Type* getType(std::string &str); //get type of unput

private:

	static bool isLegalVarName(const std::string& str); //check if the input name id legal
	static bool makeAssignment(const std::string& str);
	static Type* getVariableValue(const std::string &str);
	std::unordered_map<std::string, Type*> umap;
	

};

#endif //PARSER_H
