#include "parser.h"
#include "IndentationException.h"
#include "SyntaxException.h"
#include <iostream>
#include <sstream>
#include <string>
#include "Integer.h"
#include "Boolean.h"
#include "String.h"
#include "Helper.h"
#include "NameErrorException.h"

/*
function will check the input as a string
*/
Type* Parser::parseString(std::string str) throw()
{
	bool flag = true;
	if (str.length() > 0)
	{
		for (int i = 0; i < str.length(); i++)
		{
			if ((str[i] == 9 || str[i] == 32) && flag) //check if first char is space or tab
			{
				throw IndentationException();
			}
			else
			{
				break;
			}
		}

		Helper::rtrim(str);
		Type* t = getType(str);
		return t;
		std::cout << str << std::endl;
	}
}

//get the type of  the input
Type* Parser::getType(std::string& str)
{
	std::string strWithoutSpaces = str;
	
	Helper::trim(strWithoutSpaces);

	if (Helper::isInteger(strWithoutSpaces))
	{
		Integer* int1 = new Integer(std::stoi(strWithoutSpaces));
		(int1)->setIsTemp(true);
		return int1;
	}

	else if ((Helper::isBoolean(strWithoutSpaces)))
	{
		bool b = strWithoutSpaces == "True" ? true: false;
		Boolean* bool1 = new Boolean(b);
		bool1->setIsTemp(true);
		return bool1;
	}

	else if ((Helper::isString(strWithoutSpaces)))
	{
		String* strObj = new String(strWithoutSpaces);
		strObj->setIsTemp(true);
		return strObj;
	}

	else
	{
		throw SyntaxException();
		return NULL;
	}
}



//check if the string input is a valid variable name
bool Parser::isLegalVarName(const std::string& str)
{
	if (!Helper::isDigit(str[0]))
	{
		return false;
	}

	for (int i = 1; i < str.length(); i++)
	{
		if (!(Helper::isDigit(str[i]) || Helper::isLetter(str[i]) || str[i] == '_'))
		{
			return false;
		}
	}
	return true;
}



///////////////////////have'nt finished
bool Parser::makeAssignment(const std::string& str)
{
	bool ans = false;
	std::string leftSide;
	leftSide[0] = str[0];
	int indexOfEqual = 0;
	for (int i = 1; i < str.length() - 1; i++)
	{
		leftSide += str[i];
		if (str[i] == '=')
		{
			indexOfEqual = i;
			ans = true;
			break;
		}
	}
	if (!ans)
	{
		return ans;
	}

	ans = false;
	std::string rightSide;
	for (int i = indexOfEqual + 1; i < str.length(); i++)
	{
		rightSide += str[i];
	}
	
	Helper::ltrim(leftSide);
	Helper::rtrim(leftSide);
	Helper::ltrim(rightSide);
	Helper::rtrim(rightSide);

	if (!isLegalVarName(leftSide))
	{
		NameErrorException nameExcp(leftSide);
		throw nameExcp.what();
	}

	Type* t = getType(rightSide);
	if (t == NULL)
	{
		throw SyntaxException();
	}

}


