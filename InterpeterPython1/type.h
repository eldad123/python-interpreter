#ifndef TYPE_H
#define TYPE_H
#include <iostream>

class Type
{
public:
	void setIsTemp(bool isTemp);
	Type();
	~Type();
	bool getIsTemp();
	
	virtual const bool isPrintable() = 0;
	virtual const std::string toString() = 0;
protected:
	bool _isTemp;
};





#endif //TYPE_H
