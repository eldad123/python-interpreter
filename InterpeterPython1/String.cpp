#include "String.h"

String::String(std::string str)
{
	this->_str = str;
}

String::~String()
{

}


bool const String::isPrintable()
{
	return true;
}

std::string const String::toString()
{
	bool hasApostrophe = false;
	for (int i = 1; i < this->_str.length() - 1; i++)
	{
		if (this->_str[i] == 39)
		{
			hasApostrophe = true;
			this->_str[0] = 34;
			this->_str[this->_str.length() - 1] = 34;
			return this->_str;
		}
	}

	if (this->_str[0] == '"' && this->_str[this->_str.length() - 1] == '"')
	{
		this->_str[0] = 39;
		this->_str[this->_str.length() - 1] = 39;
	}


	return this->_str;

}