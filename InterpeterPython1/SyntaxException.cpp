#include "SyntaxException.h"

//syntax exception
const char* SyntaxException::what() const throw()
{
	return "SyntaxError: invalid syntax";
}