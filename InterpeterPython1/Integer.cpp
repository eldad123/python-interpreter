#include "Integer.h"
#include <string> 

Integer::Integer(int x)
{
	this->_x = x;
}

Integer::~Integer()
{

}

int Integer::getX()
{
	return this->_x;
}

void Integer::setX(int x)
{
	this->_x = x;
}

bool const Integer::isPrintable() 
{
	return true;
}

std::string const Integer::toString() 
{
	std::string strX = std::to_string(this->_x);
	return strX;
}