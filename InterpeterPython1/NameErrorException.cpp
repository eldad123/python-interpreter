#include "NameErrorException.h"
#include <string>

//name exception c'tor
NameErrorException::NameErrorException(const std::string str)
{
	this->_name = str;
}

//name exception
const char* NameErrorException::what() const throw()
{
	const char* exp;
	std::string expStr;
	expStr = "NameError : name " + this->_name;
	expStr += this->_name;
	expStr += " is not defined";
	exp = expStr.c_str();
	return exp;
}
